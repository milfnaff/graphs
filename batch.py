import multiprocessing as mp
from graphs import main
from itertools import product, count
from csv import DictWriter
import sys

class Args: pass

params = dict(
    n=range(4, 261, 1),
    s=[4],
    p=[0.1],
    r=[1337],
    d=[False],
)

def calculate(d):
    args = Args()
    args.__dict__.update(dict(zip(params, d)))
    return main(args)

if __name__ == "__main__":
    mp.freeze_support()
    file = sys.stdout
    writer = None
    with mp.Pool(10) as pool:
        combos = list(product(*params.values()))
        for i, output in enumerate(pool.imap(calculate, combos)):
            print(f"{i+1}/{len(combos)}\tDONE\t{combos[i]}", file=sys.stderr)
            if writer is None:
                writer = DictWriter(file, fieldnames=output, lineterminator="\n")
                writer.writeheader()
            writer.writerow(output)
            file.flush()

