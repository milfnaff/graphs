from functools import partial, wraps
from itertools import groupby, product, repeat, chain, takewhile, dropwhile
import operator
import time
from dataflow import Flow, fmap, makemap, feval, bind
import networkx as nx
import multiprocessing as mp
from sloyka import sloyka
import random

from contextlib import contextmanager
from argparse import ArgumentParser

import linecache
import os
import tracemalloc

import networkx


@contextmanager
def profile():
    tracemalloc.start()
    start = time.monotonic()
    result = {}
    yield result
    snapshot = tracemalloc.take_snapshot()
    snapshot = snapshot.filter_traces((
        tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
        tracemalloc.Filter(False, "<unknown>"),
    ))
    top_stats = snapshot.statistics("lineno")
    total = sum(stat.size for stat in top_stats)
    result["time"] = time.monotonic() - start
    result["mem"] = sum(stat.size for stat in top_stats)


def is_isomorphic(a, b) -> bool:
    return nx.is_isomorphic(nx.Graph(a), nx.Graph(b)), b


def probability(s) -> float:
    x = float(s)
    if 0 <= x <= 1:
        return x
    else:
        raise ValueError(x)

import json

parser = ArgumentParser()
parser.add_argument("-n", type=int, required=True)
parser.add_argument("-s", type=int, required=True)
parser.add_argument("-p", type=probability, required=True)
parser.add_argument("-r", type=int, required=True)
parser.add_argument("-d", action='store_true')
parser.add_argument("-j", type=int, default=None)


def do_stuff(fmap, graph, subgraph):
    with profile() as prof:
        sloycount = 0
        def sloyinc(x): nonlocal sloycount; sloycount += 1
        sloys = (
            Flow(sloyka(graph))
            >> partial(takewhile, lambda s: len(s) <= len(subgraph.edges))
            << sloyinc
            # >> fmap(partial(is_isomorphic, subgraph))
            # Flow(fmap(is_ismorphic, takewhile(sloyka(graph), lambda ...), subgraph))
        )
        flow = (
            Flow(sloys)
            >> partial(dropwhile, lambda s: len(s) < len(subgraph.edges))
            >> fmap(partial(is_isomorphic, subgraph))
        )
        count = sum(1 for isomorphic, edges in flow if isomorphic)
    return prof["mem"], prof["time"], sloycount, count


def make_graph(host, sub, p) -> nx.Graph:
    g = nx.union(host, sub, rename=("H", "S"))
    for h, s in product(host, sub):
        if random.random() <= p:
            g.add_edge(f"H{h}", f"S{s}")
    return g


def main(args, pool=None):
    random.seed(args.r)

    from networkx.generators import random_graphs, classic
    from networkx.generators.intersection import uniform_random_intersection_graph
    #hostgraph = random_graphs.random_lobster(args.n, p1=args.p, p2=args.p, seed=args.r)
    #hostgraph = random_graphs.random_regular_graph(3, args.n)
    #hostgraph = uniform_random_intersection_graph(args.n, args.n, args.p, seed=args.r)
    hostgraph = random_graphs.powerlaw_cluster_graph(args.n, 2, args.p, seed=args.r)
    subgraph = classic.star_graph(args.s, create_using=networkx.Graph)

    if args.d:
        from matplotlib import pyplot
        draw = partial(nx.draw, with_labels=True, font_size=24, node_color="#ffff00", width=1.5)
        pyplot.subplot(1, 2, 1)
        draw(hostgraph)
        pyplot.subplot(1, 2, 2)
        draw(subgraph)
        pyplot.show()

    if pool is not None:
        pmap = makemap(bind(pool.imap, chunksize=16))
    else:
        pmap = fmap

    mem, time, sloys, count = do_stuff(pmap, hostgraph, subgraph)
    return {
        "hostgraph.nodes": len(hostgraph),
        "hostgraph.edges": len(hostgraph.edges),
        "subgraph.nodes": len(subgraph),
        "subgraph.edges": len(subgraph.edges),
        "prob": args.p,
        "seed": args.r,
        "jobs": pool._processes if pool is not None else 1,
        "mem": mem,
        "time": time,
        "sloys": sloys,
        "found": count,
    }


if __name__ == "__main__":
    mp.freeze_support()
    args = parser.parse_args()
    with mp.Pool(args.j) as pool:
        print(main(args, pool))

