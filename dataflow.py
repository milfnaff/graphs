from __future__ import annotations
from collections import deque
from functools import partial


class bind(partial):
    def __call__(self, *args, **keywords):
        iargs = iter(args)
        args = (next(iargs) if arg is ... else arg for arg in self.args)
        return self.func(*args, *iargs, **{**self.keywords, **keywords})


makemap = lambda m: lambda f, *xs: partial(m, bind(f, *xs))
fmap = makemap(map)
feval = lambda f: deque(f, maxlen=0)

def foreach(f, xs):
    for x in xs:
        f(x)
        yield x

class Flow:
    def __init__(self, x, parent=None):
        self.iter = parent is None and iter(x) or x(parent)

    def __iter__(self):
        return self.iter

    def __rshift__(self, x):
        return Flow(x, parent=self)

    def __lshift__(self, x):
        return Flow(makemap(foreach)(x), parent=self)

