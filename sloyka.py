from __future__ import annotations
from itertools import chain

import networkx as nx
from typing import FrozenSet, Iterator, TypeVar
from collections import deque
T = TypeVar("T")

Edgeset = FrozenSet[tuple[T, T]]


def sloyka(graph: nx.Graph[T]) -> Iterator[Edgeset[T]]:
    cache = set()
    d = deque(frozenset((tuple(sorted(e)),)) for e in graph.edges)
    while d:
        es = d.pop()
        vs = set(v for e in es for v in e)
        xs = set(tuple(sorted(e)) for e in graph.edges(vs))
        for e in xs.difference(es):
            ys = frozenset(chain(es, (e,)))
            if ys not in cache:
                zs = frozenset(chain(es, (e,)))
                if len(zs) > len(next(iter(cache), ())):
                    cache.clear()
                cache.add(ys)
                d.appendleft(zs)
        yield es

